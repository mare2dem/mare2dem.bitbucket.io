��?A      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]�(�docutils.nodes��target���)��}�(h�.. _sect_command_line_args:�h]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��refid��sect-command-line-args�u�tagname�h
�line�K�parent�h�	_document�h�source���/var/folders/dj/ywf9rt4x4g90mjt155y6r7vh0000gn/T/tmpfki2_mdt/7b209670789dd2005207da9e9b3857117d7dc0f9/source/command_line_args.rst�ubh	�section���)��}�(hhh]�(h	�title���)��}�(h�Command Line Arguments�h]�h	�Text����Command Line Arguments�����}�(hh-h h+h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhh)h h&h!hh"h#hKubh	�	paragraph���)��}�(h�MNormally MARE2DEM is called with just the name of the input resistivity file:�h]�h0�MNormally MARE2DEM is called with just the name of the input resistivity file:�����}�(hh?h h=h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhh;h"h#hKh h&h!hubh	�block_quote���)��}�(hhh]�h<)��}�(h�.``mpirun -n <np> MARE2DEM <resistivity file>``�h]�h	�literal���)��}�(hhRh]�h0�*mpirun -n <np> MARE2DEM <resistivity file>�����}�(hhh hVh!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh hPubah}�(h]�h]�h]�h]�h]�uhh;h"h#hKh hMubah}�(h]�h]�h]�h]�h]�uhhKh h&h!hh"h#hNubh<)��}�(hX�  where ``<resistivity file>`` is the name of the required input
:Ref:`sect_resistivity_file`. By convention, this file should have the extension
``.resistivity``. For example ``inputModel.0.resistivity``.  The model found by
each inversion iteration is then output to a new resistivity file with
the iteration number incremented.  For example:
``inputModel.1.resistivity``, ``inputModel.2.resistivity``, ... The
corresponding model responses are written to ``inputModel.1.resp``,
``inputModel.2.resp``,...�h]�(h0�where �����}�(h�where �h hoh!hh"NhNubhU)��}�(h�``<resistivity file>``�h]�h0�<resistivity file>�����}�(hhh hxh!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh houbh0�# is the name of the required input
�����}�(h�# is the name of the required input
�h hoh!hh"NhNubh �pending_xref���)��}�(h�:Ref:`sect_resistivity_file`�h]�h	�inline���)��}�(hh�h]�h0�sect_resistivity_file�����}�(hhh h�h!hh"NhNubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhh�h h�ubah}�(h]�h]�h]�h]�h]��refdoc��command_line_args��	refdomain�h��reftype��ref��refexplicit���refwarn���	reftarget��sect_resistivity_file�uhh�h"h#hK
h houbh0�5. By convention, this file should have the extension
�����}�(h�5. By convention, this file should have the extension
�h hoh!hh"NhNubhU)��}�(h�``.resistivity``�h]�h0�.resistivity�����}�(hhh h�h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh houbh0�. For example �����}�(h�. For example �h hoh!hh"NhNubhU)��}�(h�``inputModel.0.resistivity``�h]�h0�inputModel.0.resistivity�����}�(hhh h�h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh houbh0��.  The model found by
each inversion iteration is then output to a new resistivity file with
the iteration number incremented.  For example:
�����}�(h��.  The model found by
each inversion iteration is then output to a new resistivity file with
the iteration number incremented.  For example:
�h hoh!hh"NhNubhU)��}�(h�``inputModel.1.resistivity``�h]�h0�inputModel.1.resistivity�����}�(hhh h�h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh houbh0�, �����}�(h�, �h hoh!hh"NhNubhU)��}�(h�``inputModel.2.resistivity``�h]�h0�inputModel.2.resistivity�����}�(hhh h�h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh houbh0�7, … The
corresponding model responses are written to �����}�(h�7, ... The
corresponding model responses are written to �h hoh!hh"NhNubhU)��}�(h�``inputModel.1.resp``�h]�h0�inputModel.1.resp�����}�(hhh j  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh houbh0�,
�����}�(h�,
�h hoh!hh"NhNubhU)��}�(h�``inputModel.2.resp``�h]�h0�inputModel.2.resp�����}�(hhh j  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh houbh0�,…�����}�(h�,...�h hoh!hh"NhNubeh}�(h]�h]�h]�h]�h]�uhh;h"h#hK
h h&h!hubh<)��}�(h��The command ``mpirun`` starts up the parallel message
passing interface environment and has argument ``-n <np>`` where ``<np>`` should be
the number of processing cores on your system (laptop, desktop, etc).�h]�(h0�The command �����}�(h�The command �h j/  h!hh"NhNubhU)��}�(h�
``mpirun``�h]�h0�mpirun�����}�(hhh j8  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh j/  ubh0�O starts up the parallel message
passing interface environment and has argument �����}�(h�O starts up the parallel message
passing interface environment and has argument �h j/  h!hh"NhNubhU)��}�(h�``-n <np>``�h]�h0�-n <np>�����}�(hhh jK  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh j/  ubh0� where �����}�(h� where �h j/  h!hh"NhNubhU)��}�(h�``<np>``�h]�h0�<np>�����}�(hhh j^  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh j/  ubh0�P should be
the number of processing cores on your system (laptop, desktop, etc).�����}�(h�P should be
the number of processing cores on your system (laptop, desktop, etc).�h j/  h!hh"NhNubeh}�(h]�h]�h]�h]�h]�uhh;h"h#hKh h&h!hubh<)��}�(hX-  For inversion models, MARE2DEM will also outputs the normalized data
sensitivities to the free parameters (see :Ref:`sect_sensitivity`).  The
sensitivity is output to the file ``<resistivity
file>.<iter#>.sensitivity``. plotMARE2DEM.m can be used to plot
the sensitivity; it can be overlain as contours or shading on inversion
models. The sensitivity can be used as a relative measure of data
sensitivity to structure, but be aware this is a linearized
approximation for what is a non-linear inverse problem, so results
should be taken with a grain of salt.�h]�(h0�oFor inversion models, MARE2DEM will also outputs the normalized data
sensitivities to the free parameters (see �����}�(h�oFor inversion models, MARE2DEM will also outputs the normalized data
sensitivities to the free parameters (see �h jw  h!hh"NhNubh�)��}�(h�:Ref:`sect_sensitivity`�h]�h�)��}�(hj�  h]�h0�sect_sensitivity�����}�(hhh j�  h!hh"NhNubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�h j�  ubah}�(h]�h]�h]�h]�h]��refdoc�h��	refdomain�j�  �reftype��ref��refexplicit���refwarn��h��sect_sensitivity�uhh�h"h#hKh jw  ubh0�*).  The
sensitivity is output to the file �����}�(h�*).  The
sensitivity is output to the file �h jw  h!hh"NhNubhU)��}�(h�*``<resistivity
file>.<iter#>.sensitivity``�h]�h0�&<resistivity
file>.<iter#>.sensitivity�����}�(hhh j�  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh jw  ubh0XS  . plotMARE2DEM.m can be used to plot
the sensitivity; it can be overlain as contours or shading on inversion
models. The sensitivity can be used as a relative measure of data
sensitivity to structure, but be aware this is a linearized
approximation for what is a non-linear inverse problem, so results
should be taken with a grain of salt.�����}�(hXS  . plotMARE2DEM.m can be used to plot
the sensitivity; it can be overlain as contours or shading on inversion
models. The sensitivity can be used as a relative measure of data
sensitivity to structure, but be aware this is a linearized
approximation for what is a non-linear inverse problem, so results
should be taken with a grain of salt.�h jw  h!hh"NhNubeh}�(h]�h]�h]�h]�h]�uhh;h"h#hKh h&h!hubh<)��}�(h�nMARE2DEM has a few other command line options for more advanced usage, but most users
won't need to use these:�h]�h0�pMARE2DEM has a few other command line options for more advanced usage, but most users
won’t need to use these:�����}�(hj�  h j�  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhh;h"h#hK!h h&h!hubhL)��}�(hhh]�h<)��}�(h�d``mpirun -n <np>  MARE2DEM [-F] [-J] [-scratch <scratchfolder>] <resistivity file> [<output file>]``�h]�hU)��}�(hj�  h]�h0�`mpirun -n <np>  MARE2DEM [-F] [-J] [-scratch <scratchfolder>] <resistivity file> [<output file>]�����}�(hhh j�  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh j�  ubah}�(h]�h]�h]�h]�h]�uhh;h"h#hK$h j�  ubah}�(h]�h]�h]�h]�h]�uhhKh h&h!hh"h#hNubh<)��}�(h�0where the arguments in brackets [] are optional:�h]�h0�0where the arguments in brackets [] are optional:�����}�(hj�  h j�  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhh;h"h#hK&h h&h!hubhL)��}�(hhh]�(h<)��}�(h��``<output file>`` - With this option, the output files are
named ``<output file>.1.resistivity``, ``<output file>.1.resp``,
``<output file>.2.resistivity``, ``<output file>.2.resp``,...�h]�(hU)��}�(h�``<output file>``�h]�h0�<output file>�����}�(hhh j  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh j�  ubh0�0 - With this option, the output files are
named �����}�(h�0 - With this option, the output files are
named �h j�  h!hh"NhNubhU)��}�(h�``<output file>.1.resistivity``�h]�h0�<output file>.1.resistivity�����}�(hhh j  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh j�  ubh0�, �����}�(h�, �h j�  h!hh"NhNubhU)��}�(h�``<output file>.1.resp``�h]�h0�<output file>.1.resp�����}�(hhh j'  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh j�  ubh0�,
�����}�(h�,
�h j�  h!hh"NhNubhU)��}�(h�``<output file>.2.resistivity``�h]�h0�<output file>.2.resistivity�����}�(hhh j:  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh j�  ubh0�, �����}�(hj&  h j�  ubhU)��}�(h�``<output file>.2.resp``�h]�h0�<output file>.2.resp�����}�(hhh jL  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh j�  ubh0�,…�����}�(h�,...�h j�  h!hh"NhNubeh}�(h]�h]�h]�h]�h]�uhh;h"h#hK(h j�  ubh<)��}�(hX	  ``-F`` Computes the forward response of the input model only. The
forward response is output to ``<resistivity file>.resp``. Note that
if MARE2DEM detects no free parameters in the input model it will
only compute the forward response; thus, typically the ``-F`` is
only required when you want to compute the forward response of an
inversion model (say when doing sensitivity tests of anomalous
features found by inversion, where you paint over the anomaly using
Mamba2D and then see how the MT or CSEM responses change).�h]�(hU)��}�(h�``-F``�h]�h0�-F�����}�(hhh ji  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh je  ubh0�Z Computes the forward response of the input model only. The
forward response is output to �����}�(h�Z Computes the forward response of the input model only. The
forward response is output to �h je  h!hh"NhNubhU)��}�(h�``<resistivity file>.resp``�h]�h0�<resistivity file>.resp�����}�(hhh j|  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh je  ubh0��. Note that
if MARE2DEM detects no free parameters in the input model it will
only compute the forward response; thus, typically the �����}�(h��. Note that
if MARE2DEM detects no free parameters in the input model it will
only compute the forward response; thus, typically the �h je  h!hh"NhNubhU)��}�(h�``-F``�h]�h0�-F�����}�(hhh j�  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh je  ubh0X   is
only required when you want to compute the forward response of an
inversion model (say when doing sensitivity tests of anomalous
features found by inversion, where you paint over the anomaly using
Mamba2D and then see how the MT or CSEM responses change).�����}�(hX   is
only required when you want to compute the forward response of an
inversion model (say when doing sensitivity tests of anomalous
features found by inversion, where you paint over the anomaly using
Mamba2D and then see how the MT or CSEM responses change).�h je  h!hh"NhNubeh}�(h]�h]�h]�h]�h]�uhh;h"h#hK,h j�  ubh<)��}�(hX  ``-J`` Outputs the full Jacobian matrix of the input model for each
iteration. This flag is provided for advanced users wishing to use
the Jacobian matrix in other calculations and normal users do not
need to use this; further, the output Jacobian files can be quite
large. The Jacobian matrix is written to an unformatted binary data
file columnwise with the naming convention ``<resistivity
file>.<iter#>.jacobianBin``. Note that the saved J is unweighted
(i.e. it has NOT been normalized by the data uncertainties).�h]�(hU)��}�(h�``-J``�h]�h0�-J�����}�(hhh j�  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh j�  ubh0Xt   Outputs the full Jacobian matrix of the input model for each
iteration. This flag is provided for advanced users wishing to use
the Jacobian matrix in other calculations and normal users do not
need to use this; further, the output Jacobian files can be quite
large. The Jacobian matrix is written to an unformatted binary data
file columnwise with the naming convention �����}�(hXt   Outputs the full Jacobian matrix of the input model for each
iteration. This flag is provided for advanced users wishing to use
the Jacobian matrix in other calculations and normal users do not
need to use this; further, the output Jacobian files can be quite
large. The Jacobian matrix is written to an unformatted binary data
file columnwise with the naming convention �h j�  h!hh"NhNubhU)��}�(h�*``<resistivity
file>.<iter#>.jacobianBin``�h]�h0�&<resistivity
file>.<iter#>.jacobianBin�����}�(hhh j�  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh j�  ubh0�b. Note that the saved J is unweighted
(i.e. it has NOT been normalized by the data uncertainties).�����}�(h�b. Note that the saved J is unweighted
(i.e. it has NOT been normalized by the data uncertainties).�h j�  h!hh"NhNubeh}�(h]�h]�h]�h]�h]�uhh;h"h#hK6h j�  ubh<)��}�(h��``-scratch <scratchfolder>``  Use the specified directory for the scratch files
required for 2.5D CSEM inversion (but not needed for MT). Optimally
this should be a local directory on each compute node and not
a networked directory.�h]�(hU)��}�(h�``-scratch <scratchfolder>``�h]�h0�-scratch <scratchfolder>�����}�(hhh j�  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhTh j�  ubh0��  Use the specified directory for the scratch files
required for 2.5D CSEM inversion (but not needed for MT). Optimally
this should be a local directory on each compute node and not
a networked directory.�����}�(h��  Use the specified directory for the scratch files
required for 2.5D CSEM inversion (but not needed for MT). Optimally
this should be a local directory on each compute node and not
a networked directory.�h j�  h!hh"NhNubeh}�(h]�h]�h]�h]�h]�uhh;h"h#hK?h j�  ubeh}�(h]�h]�h]�h]�h]�uhhKh h&h!hh"h#hNubeh}�(h]�(�command-line-arguments�heh]�h]�(�command line arguments��sect_command_line_args�eh]�h]�uhh$h hh!hh"h#hK�expect_referenced_by_name�}�j  hs�expect_referenced_by_id�}�hhsubeh}�(h]�h]�h]�h]�h]��source�h#uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h)N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j+  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h#�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_images���embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}�h]�has�nameids�}�(j  hj   j�  u�	nametypes�}�(j  �j   Nuh}�(hh&j�  h&u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]�h	�system_message���)��}�(hhh]�h<)��}�(hhh]�h0�<Hyperlink target "sect-command-line-args" is not referenced.�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhh;h j�  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h#�line�Kuhj�  uba�transformer�N�include_log�]��
decoration�Nh!hub.