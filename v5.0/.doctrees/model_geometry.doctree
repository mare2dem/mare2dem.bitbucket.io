���      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]�(�docutils.nodes��target���)��}�(h�.. _sect_model_geometry:�h]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��refid��sect-model-geometry�u�tagname�h
�line�K�parent�h�	_document�h�source��/var/folders/dj/ywf9rt4x4g90mjt155y6r7vh0000gn/T/tmpfki2_mdt/7b209670789dd2005207da9e9b3857117d7dc0f9/source/model_geometry.rst�ubh	�section���)��}�(hhh]�(h	�title���)��}�(h�Model Geometry�h]�h	�Text����Model Geometry�����}�(hh-h h+h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhh)h h&h!hh"h#hKubh	�	paragraph���)��}�(h��MARE2DEM uses a right-handed coordinate system with the *z* axis
positive down. *x* is the model strike direction where conductivity is
invariant, so :math:`{\sigma} = {\sigma(y,z)}`.�h]�(h0�8MARE2DEM uses a right-handed coordinate system with the �����}�(h�8MARE2DEM uses a right-handed coordinate system with the �h h=h!hh"NhNubh	�emphasis���)��}�(h�*z*�h]�h0�z�����}�(hhh hHh!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhFh h=ubh0� axis
positive down. �����}�(h� axis
positive down. �h h=h!hh"NhNubhG)��}�(h�*x*�h]�h0�x�����}�(hhh h[h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhFh h=ubh0�C is the model strike direction where conductivity is
invariant, so �����}�(h�C is the model strike direction where conductivity is
invariant, so �h h=h!hh"NhNubh	�math���)��}�(h� :math:`{\sigma} = {\sigma(y,z)}`�h]�h0�{\sigma} = {\sigma(y,z)}�����}�(hhh hph!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhnh h=ubh0�.�����}�(h�.�h h=h!hh"NhNubeh}�(h]�h]�h]�h]�h]�uhh;h"h#hKh h&h!hubh	�note���)��}�(hX  There is no assumption about where :math:`z=0` resides in the model or
data spaces, although generally it is recommended to set :math:`z=0` to
be sea level. Similarly, transmitters and receivers
can be located at any *x* position, but generally they should be close
to :math:`x=0`.�h]�h<)��}�(hX  There is no assumption about where :math:`z=0` resides in the model or
data spaces, although generally it is recommended to set :math:`z=0` to
be sea level. Similarly, transmitters and receivers
can be located at any *x* position, but generally they should be close
to :math:`x=0`.�h]�(h0�#There is no assumption about where �����}�(h�#There is no assumption about where �h h�h!hh"NhNubho)��}�(h�:math:`z=0`�h]�h0�z=0�����}�(hhh h�h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhnh h�ubh0�R resides in the model or
data spaces, although generally it is recommended to set �����}�(h�R resides in the model or
data spaces, although generally it is recommended to set �h h�h!hh"NhNubho)��}�(h�:math:`z=0`�h]�h0�z=0�����}�(hhh h�h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhnh h�ubh0�N to
be sea level. Similarly, transmitters and receivers
can be located at any �����}�(h�N to
be sea level. Similarly, transmitters and receivers
can be located at any �h h�h!hh"NhNubhG)��}�(h�*x*�h]�h0�x�����}�(hhh h�h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhFh h�ubh0�1 position, but generally they should be close
to �����}�(h�1 position, but generally they should be close
to �h h�h!hh"NhNubho)��}�(h�:math:`x=0`�h]�h0�x=0�����}�(hhh h�h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhnh h�ubh0�.�����}�(hh�h h�h!hh"NhNubeh}�(h]�h]�h]�h]�h]�uhh;h"h#hKh h�ubah}�(h]�h]�h]�h]�h]�uhh�h h&h!hh"h#hNubh	�figure���)��}�(hhh]�(h	�image���)��}�(hXI  .. figure::  _static/images/model_geometry.png
    :width: 50%

    Cross sectional view of the right-handed coordinate system used in
    MARE2DEM. As the right-hand name implies, if you point your
    right index finger along *x* and your right middle finger along *y*,
    your right thumb will point down along the *z* axis.
�h]�h}�(h]�h]�h]�h]�h]��width��50%��uri��!_static/images/model_geometry.png��
candidates�}��*�j  suhh�h h�h"h#hKubh	�caption���)��}�(h��Cross sectional view of the right-handed coordinate system used in
MARE2DEM. As the right-hand name implies, if you point your
right index finger along *x* and your right middle finger along *y*,
your right thumb will point down along the *z* axis.�h]�(h0��Cross sectional view of the right-handed coordinate system used in
MARE2DEM. As the right-hand name implies, if you point your
right index finger along �����}�(h��Cross sectional view of the right-handed coordinate system used in
MARE2DEM. As the right-hand name implies, if you point your
right index finger along �h j	  h!hh"NhNubhG)��}�(h�*x*�h]�h0�x�����}�(hhh j  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhFh j	  ubh0�$ and your right middle finger along �����}�(h�$ and your right middle finger along �h j	  h!hh"NhNubhG)��}�(h�*y*�h]�h0�y�����}�(hhh j%  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhFh j	  ubh0�-,
your right thumb will point down along the �����}�(h�-,
your right thumb will point down along the �h j	  h!hh"NhNubhG)��}�(h�*z*�h]�h0�z�����}�(hhh j8  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhhFh j	  ubh0� axis.�����}�(h� axis.�h j	  h!hh"NhNubeh}�(h]�h]�h]�h]�h]�uhj  h"h#hKh h�ubeh}�(h]��id1�ah]�h]�h]�h]�uhh�hKh h&h!hh"h#ubh�)��}�(hhh]�(h�)��}�(h��.. figure::  _static/images/model_geometry_map.png
    :width: 50%

    Map view of the right-handed coordinate system used in MARE2DEM.
�h]�h}�(h]�h]�h]�h]�h]��width��50%��uri��%_static/images/model_geometry_map.png�j  }�j  jh  suhh�h jX  h"h#hKubj  )��}�(h�@Map view of the right-handed coordinate system used in MARE2DEM.�h]�h0�@Map view of the right-handed coordinate system used in MARE2DEM.�����}�(hjl  h jj  h!hh"NhNubah}�(h]�h]�h]�h]�h]�uhj  h"h#hKh jX  ubeh}�(h]��id2�ah]�h]�h]�h]�uhh�hKh h&h!hh"h#ubeh}�(h]�(�model-geometry�heh]�h]�(�model geometry��sect_model_geometry�eh]�h]�uhh$h hh!hh"h#hK�expect_referenced_by_name�}�j�  hs�expect_referenced_by_id�}�hhsubeh}�(h]�h]�h]�h]�h]��source�h#uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h)N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h#�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_images���embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}�h]�has�nameids�}�(j�  hj�  j�  u�	nametypes�}�(j�  �j�  Nuh}�(hh&j�  h&jS  h�jz  jX  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}�j�  Ks��R��parse_messages�]��transform_messages�]�h	�system_message���)��}�(hhh]�h<)��}�(hhh]�h0�9Hyperlink target "sect-model-geometry" is not referenced.�����}�(hhh j  ubah}�(h]�h]�h]�h]�h]�uhh;h j  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h#�line�Kuhj  uba�transformer�N�include_log�]��
decoration�Nh!hub.