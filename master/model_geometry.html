<!DOCTYPE html>
<html class="writer-html5" lang="en" >
<head>
  <meta charset="utf-8" /><meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Model Geometry &mdash; MARE2DEM Documentation v5.1.2 documentation</title>
      <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
      <link rel="stylesheet" href="_static/css/theme.css" type="text/css" />
      <link rel="stylesheet" href="_static/css/custom.css" type="text/css" />
    <link rel="shortcut icon" href="_static/favicon.png"/>
  <!--[if lt IE 9]>
    <script src="_static/js/html5shiv.min.js"></script>
  <![endif]-->
  
        <script data-url_root="./" id="documentation_options" src="_static/documentation_options.js"></script>
        <script src="_static/jquery.js"></script>
        <script src="_static/underscore.js"></script>
        <script src="_static/doctools.js"></script>
        <script async="async" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <script src="_static/js/theme.js"></script>
    <link rel="author" title="About these documents" href="about.html" />
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Data Geometry" href="data_geometry.html" />
    <link rel="prev" title="Geometry" href="geometry.html" /> 
</head>

<body class="wy-body-for-nav"> 
  <div class="wy-grid-for-nav">
    <nav data-toggle="wy-nav-shift" class="wy-nav-side">
      <div class="wy-side-scroll">
        <div class="wy-side-nav-search" >
            <a href="index.html">
            <img src="_static/logo1.png" class="logo" alt="Logo"/>
          </a>
<div role="search">
  <form id="rtd-search-form" class="wy-form" action="search.html" method="get">
    <input type="text" name="q" placeholder="Search docs" />
    <input type="hidden" name="check_keywords" value="yes" />
    <input type="hidden" name="area" value="default" />
  </form>
</div>
        </div><div class="wy-menu wy-menu-vertical" data-spy="affix" role="navigation" aria-label="Navigation menu">
              <p class="caption" role="heading"><span class="caption-text">User Manual</span></p>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="about.html">About</a></li>
<li class="toctree-l1"><a class="reference internal" href="overview.html">Workflow Overview</a></li>
<li class="toctree-l1"><a class="reference internal" href="examples.html">Example Applications</a></li>
<li class="toctree-l1"><a class="reference internal" href="install.html">Installation</a></li>
<li class="toctree-l1"><a class="reference internal" href="download.html">Download</a></li>
<li class="toctree-l1 current"><a class="reference internal" href="geometry.html">Geometry</a><ul class="current">
<li class="toctree-l2 current"><a class="current reference internal" href="#">Model Geometry</a></li>
<li class="toctree-l2"><a class="reference internal" href="data_geometry.html">Data Geometry</a></li>
<li class="toctree-l2"><a class="reference internal" href="anisotropy.html">Anisotropic Conductivity</a></li>
<li class="toctree-l2"><a class="reference internal" href="coordinate_transforms.html">Coordinate Transforms for Getting Data Into MARE2DEM</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="file_formats.html">File Formats</a></li>
<li class="toctree-l1"><a class="reference internal" href="command_line_args.html">Command Line Arguments</a></li>
<li class="toctree-l1"><a class="reference internal" href="scratch.html">Scratch Folder for CSEM Inversions</a></li>
<li class="toctree-l1"><a class="reference internal" href="colormaps.html">Colormaps</a></li>
<li class="toctree-l1"><a class="reference internal" href="tips.html">Tips</a></li>
<li class="toctree-l1"><a class="reference internal" href="theory.html">Background Theory</a></li>
<li class="toctree-l1"><a class="reference internal" href="references.html">References</a></li>
</ul>

        </div>
      </div>
    </nav>

    <section data-toggle="wy-nav-shift" class="wy-nav-content-wrap"><nav class="wy-nav-top" aria-label="Mobile navigation menu" >
          <i data-toggle="wy-nav-top" class="fa fa-bars"></i>
          <a href="index.html">MARE2DEM Documentation</a>
      </nav>

      <div class="wy-nav-content">
        <div class="rst-content">
          <div role="navigation" aria-label="Page navigation">
  <ul class="wy-breadcrumbs">
      <li><a href="index.html" class="icon icon-home"></a> &raquo;</li>
          <li><a href="geometry.html">Geometry</a> &raquo;</li>
      <li>Model Geometry</li>
      <li class="wy-breadcrumbs-aside">
            <a href="_sources/model_geometry.rst.txt" rel="nofollow"> View page source</a>
      </li>
  </ul><div class="rst-breadcrumbs-buttons" role="navigation" aria-label="Sequential page navigation">
        <a href="geometry.html" class="btn btn-neutral float-left" title="Geometry" accesskey="p"><span class="fa fa-arrow-circle-left" aria-hidden="true"></span> Previous</a>
        <a href="data_geometry.html" class="btn btn-neutral float-right" title="Data Geometry" accesskey="n">Next <span class="fa fa-arrow-circle-right" aria-hidden="true"></span></a>
  </div>
  <hr/>
</div>
          <div role="main" class="document" itemscope="itemscope" itemtype="http://schema.org/Article">
           <div itemprop="articleBody">
             


  <section id="model-geometry">
<span id="sect-model-geometry"></span><h1>Model Geometry<a class="headerlink" href="#model-geometry" title="Permalink to this headline"></a></h1>
<p>MARE2DEM uses a right-handed coordinate system with the <em>z</em> axis
positive down. <em>x</em> is the model strike direction where conductivity is
invariant, so <span class="math notranslate nohighlight">\({\sigma} = {\sigma(y,z)}\)</span>.</p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>There is no assumption about where <span class="math notranslate nohighlight">\(z=0\)</span> resides in the model or
data spaces, although generally it is recommended to set <span class="math notranslate nohighlight">\(z=0\)</span> to
be sea level. Similarly, transmitters and receivers
can be located at any <em>x</em> position, but generally they should be close
to <span class="math notranslate nohighlight">\(x=0\)</span>.</p>
</div>
<figure class="align-default" id="id1">
<a class="reference internal image-reference" href="_images/model_geometry.png"><img alt="_images/model_geometry.png" src="_images/model_geometry.png" style="width: 50%;" /></a>
<figcaption>
<p><span class="caption-number">Fig. 25 </span><span class="caption-text">Cross sectional view of the right-handed coordinate system used in
MARE2DEM. As the right-hand name implies, if you point your
right index finger along <em>x</em> and your right middle finger along <em>y</em>,
your right thumb will point down along the <em>z</em> axis.</span><a class="headerlink" href="#id1" title="Permalink to this image"></a></p>
</figcaption>
</figure>
<figure class="align-default" id="id2">
<a class="reference internal image-reference" href="_images/model_geometry_map.png"><img alt="_images/model_geometry_map.png" src="_images/model_geometry_map.png" style="width: 50%;" /></a>
<figcaption>
<p><span class="caption-number">Fig. 26 </span><span class="caption-text">Map view of the right-handed coordinate system used in MARE2DEM.</span><a class="headerlink" href="#id2" title="Permalink to this image"></a></p>
</figcaption>
</figure>
</section>



           </div>
          </div>
          <footer><div class="rst-footer-buttons" role="navigation" aria-label="Footer">
        <a href="geometry.html" class="btn btn-neutral float-left" title="Geometry" accesskey="p" rel="prev"><span class="fa fa-arrow-circle-left" aria-hidden="true"></span> Previous</a>
        <a href="data_geometry.html" class="btn btn-neutral float-right" title="Data Geometry" accesskey="n" rel="next">Next <span class="fa fa-arrow-circle-right" aria-hidden="true"></span></a>
    </div>

  <hr/>

  <div role="contentinfo">
    <p>&#169; Copyright 2015-2022, The MARE2DEM developers.</p>
  </div>

  Built with <a href="https://www.sphinx-doc.org/">Sphinx</a> using a
    <a href="https://github.com/readthedocs/sphinx_rtd_theme">theme</a>
    provided by <a href="https://readthedocs.org">Read the Docs</a>.
   

</footer>
        </div>
      </div>
    </section>
  </div>
  
<div class="rst-versions" data-toggle="rst-versions" role="note" aria-label="versions">
  <span class="rst-current-version" data-toggle="rst-current-version">
    <span class="fa fa-book"> MARE2DEM Versions</span>
    v: master
    <span class="fa fa-caret-down"></span>
  </span>
  <div class="rst-other-versions">
    <dl>
      <dt>Tags</dt>
      <dd><a href="../v0.1/model_geometry.html">v0.1</a></dd>
      <dd><a href="../v5.0/model_geometry.html">v5.0</a></dd>
      <dd><a href="../v5.2/model_geometry.html">v5.2</a></dd>
    </dl>
    <dl>
      <dt>Branches</dt>
      <dd><a href="../dev/model_geometry.html">dev</a></dd>
      <dd><a href="model_geometry.html">master</a></dd>
    </dl>
  </div>
</div>
 <script>
      jQuery(function () {
          SphinxRtdTheme.Navigation.enable(true);
      });
  </script> 

</body>
</html>