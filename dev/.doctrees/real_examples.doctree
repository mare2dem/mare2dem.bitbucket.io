��/p      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�	Real Data�h]�h	�Text����	Real Data�����}�(hh�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�~/var/folders/dj/ywf9rt4x4g90mjt155y6r7vh0000gn/T/tmpfki2_mdt/19288fd89060e090af7e176e6b7d8e13d2f5fec9/source/real_examples.rst�hKubh	�	paragraph���)��}�(hX�  The sections below show published real data applications of
MARE2DEM for inverting magnetotelluric and controlled source
electromagnetic data. All figures shown here were made using MARE2DEM's
MATLAB plotting tools, which support various functions such as
interpolated shading, resistivity contours, sensitivity contours,
sensitivity masking, well-log overlays, seismic overlays, a wide array
of color maps, and many other customizable appearance options.�h]�hX�  The sections below show published real data applications of
MARE2DEM for inverting magnetotelluric and controlled source
electromagnetic data. All figures shown here were made using MARE2DEM’s
MATLAB plotting tools, which support various functions such as
interpolated shading, resistivity contours, sensitivity contours,
sensitivity masking, well-log overlays, seismic overlays, a wide array
of color maps, and many other customizable appearance options.�����}�(hh1hh/hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh.)��}�(hX�  Note that the seawater and air layers (which are fixed parameters) have been
turned off in the plots for marine models below. Also, only the central
region of interest is shown for each model; the entire model domains
extend 100's or 1000's of km beyond the central region of interest shown
in the figures, where these outer padding regions ensure that model boundary
reflections do not corrupt the finite element solutions in the region of
interest.�h]�hX�  Note that the seawater and air layers (which are fixed parameters) have been
turned off in the plots for marine models below. Also, only the central
region of interest is shown for each model; the entire model domains
extend 100’s or 1000’s of km beyond the central region of interest shown
in the figures, where these outer padding regions ensure that model boundary
reflections do not corrupt the finite element solutions in the region of
interest.�����}�(hh?hh=hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh)��}�(hhh]�(h)��}�(h�$East Pacific Rise Mid-Ocean Ridge MT�h]�h�$East Pacific Rise Mid-Ocean Ridge MT�����}�(hhPhhNhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhhKhhhh,hKubh	�block_quote���)��}�(hhh]�(h	�figure���)��}�(hhh]�(h	�image���)��}�(hX\  .. figure::  _static/images/examples/EPR_2_ss_tri.32.resistivity_grid.png
   :width: 80%

   Example of using an unstructured grid of triangular free parameters to
   invert seafloor MT data from the East Pacific Rise spreading ridge
   at 9º 30' N, revealing a conductive region of upper mantle upwelling
   and partial melting. Small triangular parameters are used at the seafloor to
   allow for near-surface variations and to accomodate rugged
   bathymetry, while deeper in the mantle larger triangles are used. Inverted white
   triangles along the seafloor show the MT receiver locations.
   Modified from: Key, K., Constable, S., Liu, L., & Pommier, A. (2013),
   Electrical image of passive mantle upwelling beneath the northern
   East Pacific Rise. Nature, 495(7442), 499–502, DOI:
   `10.1038/nature11932 <http://doi.org/10.1038/nature11932>`_.
�h]�h}�(h!]�h#]�h%]�h']�h)]��width��80%��uri��<_static/images/examples/EPR_2_ss_tri.32.resistivity_grid.png��
candidates�}��*�husuh+hfhhchh,hK ubh	�caption���)��}�(hX�  Example of using an unstructured grid of triangular free parameters to
invert seafloor MT data from the East Pacific Rise spreading ridge
at 9º 30' N, revealing a conductive region of upper mantle upwelling
and partial melting. Small triangular parameters are used at the seafloor to
allow for near-surface variations and to accomodate rugged
bathymetry, while deeper in the mantle larger triangles are used. Inverted white
triangles along the seafloor show the MT receiver locations.
Modified from: Key, K., Constable, S., Liu, L., & Pommier, A. (2013),
Electrical image of passive mantle upwelling beneath the northern
East Pacific Rise. Nature, 495(7442), 499–502, DOI:
`10.1038/nature11932 <http://doi.org/10.1038/nature11932>`_.�h]�(hX�  Example of using an unstructured grid of triangular free parameters to
invert seafloor MT data from the East Pacific Rise spreading ridge
at 9º 30’ N, revealing a conductive region of upper mantle upwelling
and partial melting. Small triangular parameters are used at the seafloor to
allow for near-surface variations and to accomodate rugged
bathymetry, while deeper in the mantle larger triangles are used. Inverted white
triangles along the seafloor show the MT receiver locations.
Modified from: Key, K., Constable, S., Liu, L., & Pommier, A. (2013),
Electrical image of passive mantle upwelling beneath the northern
East Pacific Rise. Nature, 495(7442), 499–502, DOI:
�����}�(hX�  Example of using an unstructured grid of triangular free parameters to
invert seafloor MT data from the East Pacific Rise spreading ridge
at 9º 30' N, revealing a conductive region of upper mantle upwelling
and partial melting. Small triangular parameters are used at the seafloor to
allow for near-surface variations and to accomodate rugged
bathymetry, while deeper in the mantle larger triangles are used. Inverted white
triangles along the seafloor show the MT receiver locations.
Modified from: Key, K., Constable, S., Liu, L., & Pommier, A. (2013),
Electrical image of passive mantle upwelling beneath the northern
East Pacific Rise. Nature, 495(7442), 499–502, DOI:
�hh{hhhNhNubh	�	reference���)��}�(h�;`10.1038/nature11932 <http://doi.org/10.1038/nature11932>`_�h]�h�10.1038/nature11932�����}�(h�10.1038/nature11932�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��name�h��refuri��"http://doi.org/10.1038/nature11932�uh+h�hh{ubh	�target���)��}�(h�% <http://doi.org/10.1038/nature11932>�h]�h}�(h!]��nature11932�ah#]�h%]��10.1038/nature11932�ah']�h)]��refuri�h�uh+h��
referenced�Khh{ubh�.�����}�(h�.�hh{hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+hyhh,hKhhcubeh}�(h!]��id3�ah#]�h%]�h']�h)]�uh+hahKhh^ubhb)��}�(hhh]�(hg)��}�(hX�  .. figure::  _static/images/examples/EPR_2_ss_tri.32.resistivity_femesh.png
   :width: 80%

   Behind the scenes, MARE2DEM generates finite element (FE) meshes for
   the forward calculations of the EM fields. These unstructured
   triangular finite element meshes are automatically generated and
   adaptively refined using a goal-oriented a posteriori error
   estimator for the FE solution. This image shows an example FE mesh
   (thin black lines) near the seafloor for the model above. Notice how
   the FE mesh conform to the free and fixed parameter polygons (thick lines).
   The FE mesh shows the most refinement near the MT stations, where fine meshing
   ensures accurate electromagnetic fields are computed.

�h]�h}�(h!]�h#]�h%]�h']�h)]��width��80%��uri��>_static/images/examples/EPR_2_ss_tri.32.resistivity_femesh.png�hv}�hxh�suh+hfhh�hh,hK ubhz)��}�(hXX  Behind the scenes, MARE2DEM generates finite element (FE) meshes for
the forward calculations of the EM fields. These unstructured
triangular finite element meshes are automatically generated and
adaptively refined using a goal-oriented a posteriori error
estimator for the FE solution. This image shows an example FE mesh
(thin black lines) near the seafloor for the model above. Notice how
the FE mesh conform to the free and fixed parameter polygons (thick lines).
The FE mesh shows the most refinement near the MT stations, where fine meshing
ensures accurate electromagnetic fields are computed.�h]�hXX  Behind the scenes, MARE2DEM generates finite element (FE) meshes for
the forward calculations of the EM fields. These unstructured
triangular finite element meshes are automatically generated and
adaptively refined using a goal-oriented a posteriori error
estimator for the FE solution. This image shows an example FE mesh
(thin black lines) near the seafloor for the model above. Notice how
the FE mesh conform to the free and fixed parameter polygons (thick lines).
The FE mesh shows the most refinement near the MT stations, where fine meshing
ensures accurate electromagnetic fields are computed.�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hyhh,hK+hh�ubeh}�(h!]��id4�ah#]�h%]�h']�h)]�uh+hahK+hh^ubeh}�(h!]�h#]�h%]�h']�h)]�uh+h\hhKhhhNhNubeh}�(h!]��$east-pacific-rise-mid-ocean-ridge-mt�ah#]�h%]��$east pacific rise mid-ocean ridge mt�ah']�h)]�uh+h
hhhhhh,hKubh)��}�(hhh]�(h)��}�(h�5Middle American Trench Offshore Nicaragua MT and CSEM�h]�h�5Middle American Trench Offshore Nicaragua MT and CSEM�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh�hhhh,hK7ubh])��}�(hhh]�(hb)��}�(hhh]�hg)��}�(h�S.. figure::  _static/images/examples/aniso_0p75x.10.resistivity.png
   :width: 100%�h]�h}�(h!]�h#]�h%]�h']�h)]��width��100%��uri��6_static/images/examples/aniso_0p75x.10.resistivity.png�hv}�hxj  suh+hfhj  hh,hK ubah}�(h!]�h#]�h%]�h']�h)]�uh+hahj   ubhb)��}�(hhh]�(hg)��}�(hX�  .. figure::  _static/images/examples/aniso_0p75x.10.resistivity_ratio.png
   :width: 100%

   Example of using an unstructured grid of quadrilateral parameters to
   invert seafloor MT data from the Middle America Trench offshore
   Nicaragua for triaxially anisotropic conductivity, revealing an
   anisotropic conductive  asthenosphere layer consistent with
   partially molten mantle. Upper panel shows the *y* component of
   resistivity and the lower panel shows the *y/x* resistivity
   anisotropy ratio. Modified from: Naif, S., Key, K., Constable, S., &
   Evans, R. L. (2013), Melt-rich channel observed at the
   lithosphere-asthenosphere boundary. Nature, 495(7441), 356–359, DOI:
   `10.1038/nature11939 <http://doi.org/10.1038/nature11939>`_.

�h]�h}�(h!]�h#]�h%]�h']�h)]��width��100%��uri��<_static/images/examples/aniso_0p75x.10.resistivity_ratio.png�hv}�hxj+  suh+hfhj  hh,hK ubhz)��}�(hX|  Example of using an unstructured grid of quadrilateral parameters to
invert seafloor MT data from the Middle America Trench offshore
Nicaragua for triaxially anisotropic conductivity, revealing an
anisotropic conductive  asthenosphere layer consistent with
partially molten mantle. Upper panel shows the *y* component of
resistivity and the lower panel shows the *y/x* resistivity
anisotropy ratio. Modified from: Naif, S., Key, K., Constable, S., &
Evans, R. L. (2013), Melt-rich channel observed at the
lithosphere-asthenosphere boundary. Nature, 495(7441), 356–359, DOI:
`10.1038/nature11939 <http://doi.org/10.1038/nature11939>`_.�h]�(hX0  Example of using an unstructured grid of quadrilateral parameters to
invert seafloor MT data from the Middle America Trench offshore
Nicaragua for triaxially anisotropic conductivity, revealing an
anisotropic conductive  asthenosphere layer consistent with
partially molten mantle. Upper panel shows the �����}�(hX0  Example of using an unstructured grid of quadrilateral parameters to
invert seafloor MT data from the Middle America Trench offshore
Nicaragua for triaxially anisotropic conductivity, revealing an
anisotropic conductive  asthenosphere layer consistent with
partially molten mantle. Upper panel shows the �hj-  hhhNhNubh	�emphasis���)��}�(h�*y*�h]�h�y�����}�(hhhj8  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+j6  hj-  ubh�8 component of
resistivity and the lower panel shows the �����}�(h�8 component of
resistivity and the lower panel shows the �hj-  hhhNhNubj7  )��}�(h�*y/x*�h]�h�y/x�����}�(hhhjK  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+j6  hj-  ubh�� resistivity
anisotropy ratio. Modified from: Naif, S., Key, K., Constable, S., &
Evans, R. L. (2013), Melt-rich channel observed at the
lithosphere-asthenosphere boundary. Nature, 495(7441), 356–359, DOI:
�����}�(h�� resistivity
anisotropy ratio. Modified from: Naif, S., Key, K., Constable, S., &
Evans, R. L. (2013), Melt-rich channel observed at the
lithosphere-asthenosphere boundary. Nature, 495(7441), 356–359, DOI:
�hj-  hhhNhNubh�)��}�(h�;`10.1038/nature11939 <http://doi.org/10.1038/nature11939>`_�h]�h�10.1038/nature11939�����}�(h�10.1038/nature11939�hj^  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��name�jf  h��"http://doi.org/10.1038/nature11939�uh+h�hj-  ubh�)��}�(h�% <http://doi.org/10.1038/nature11939>�h]�h}�(h!]��nature11939�ah#]�h%]��10.1038/nature11939�ah']�h)]��refuri�jn  uh+h�h�Khj-  ubh�.�����}�(hh�hj-  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+hyhh,hK?hj  ubeh}�(h!]��id5�ah#]�h%]�h']�h)]�uh+hahK?hj   ubhb)��}�(hhh]�(hg)��}�(hX�  .. figure::  _static/images/examples/Serpent1.15.resistivity.png
   :width: 100%

   Inversion of marine CSEM data crossing the Middle America Trench.
   Shaded colors and contours show resistivity while the gray masked
   region in the seafloor masks areas the data are insensitivity to, as
   determined by the normalized Jacobian matrix (see
   :Ref:`sect_sensitivity`) . Modified from [Key16]_ and Naif, S., Key,
   K., Constable, S., & Evans, R. L. (2016), Porosity and fluid budget
   of a water‐rich megathrust revealed with electromagnetic data at the
   Middle America Trench. Geochemistry Geophysics Geosystems, 17(11),
   4495–4516, DOI: `10.1002/2016GC006556
   <http://doi.org/10.1002/2016GC006556>`_.
�h]�h}�(h!]�h#]�h%]�h']�h)]��width��100%��uri��3_static/images/examples/Serpent1.15.resistivity.png�hv}�hxj�  suh+hfhj�  hh,hK ubhz)��}�(hX^  Inversion of marine CSEM data crossing the Middle America Trench.
Shaded colors and contours show resistivity while the gray masked
region in the seafloor masks areas the data are insensitivity to, as
determined by the normalized Jacobian matrix (see
:Ref:`sect_sensitivity`) . Modified from [Key16]_ and Naif, S., Key,
K., Constable, S., & Evans, R. L. (2016), Porosity and fluid budget
of a water‐rich megathrust revealed with electromagnetic data at the
Middle America Trench. Geochemistry Geophysics Geosystems, 17(11),
4495–4516, DOI: `10.1002/2016GC006556
<http://doi.org/10.1002/2016GC006556>`_.�h]�(h��Inversion of marine CSEM data crossing the Middle America Trench.
Shaded colors and contours show resistivity while the gray masked
region in the seafloor masks areas the data are insensitivity to, as
determined by the normalized Jacobian matrix (see
�����}�(h��Inversion of marine CSEM data crossing the Middle America Trench.
Shaded colors and contours show resistivity while the gray masked
region in the seafloor masks areas the data are insensitivity to, as
determined by the normalized Jacobian matrix (see
�hj�  hhhNhNubh �pending_xref���)��}�(h�:Ref:`sect_sensitivity`�h]�h	�inline���)��}�(hj�  h]�h�sect_sensitivity�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�(�xref��std��std-ref�eh%]�h']�h)]�uh+j�  hj�  ubah}�(h!]�h#]�h%]�h']�h)]��refdoc��real_examples��	refdomain�j�  �reftype��ref��refexplicit���refwarn���	reftarget��sect_sensitivity�uh+j�  hh,hKNhj�  ubh�) . Modified from �����}�(h�) . Modified from �hj�  hhhNhNubj�  )��}�(h�Key16�h]�j�  )��}�(hj�  h]�h�[Key16]�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj�  ubah}�(h!]��id1�ah#]�h%]�h']�h)]��	refdomain��citation��reftype��ref��	reftarget�j�  �refwarn���support_smartquotes��uh+j�  hh,hKNhj�  hhubh�� and Naif, S., Key,
K., Constable, S., & Evans, R. L. (2016), Porosity and fluid budget
of a water‐rich megathrust revealed with electromagnetic data at the
Middle America Trench. Geochemistry Geophysics Geosystems, 17(11),
4495–4516, DOI: �����}�(h�� and Naif, S., Key,
K., Constable, S., & Evans, R. L. (2016), Porosity and fluid budget
of a water‐rich megathrust revealed with electromagnetic data at the
Middle America Trench. Geochemistry Geophysics Geosystems, 17(11),
4495–4516, DOI: �hj�  hhhNhNubh�)��}�(h�=`10.1002/2016GC006556
<http://doi.org/10.1002/2016GC006556>`_�h]�h�10.1002/2016GC006556�����}�(h�10.1002/2016GC006556�hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��name�j   h��#http://doi.org/10.1002/2016GC006556�uh+h�hj�  ubh�)��}�(h�&
<http://doi.org/10.1002/2016GC006556>�h]�h}�(h!]��gc006556�ah#]�h%]��10.1002/2016gc006556�ah']�h)]��refuri�j  uh+h�h�Khj�  ubh�.�����}�(hh�hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+hyhh,hKNhj�  ubeh}�(h!]��id6�ah#]�h%]�h']�h)]�uh+hahKNhj   ubhb)��}�(hhh]�(hg)��}�(h��.. figure::  _static/images/examples/Serpent1.15.sensitivity.png
   :width: 100%

   A similar plot to the one above, but now with shaded colors showing
   the log10 sensitivity (see :Ref:`sect_sensitivity`) and contours
   showing log10 resistivity.

�h]�h}�(h!]�h#]�h%]�h']�h)]��width��100%��uri��3_static/images/examples/Serpent1.15.sensitivity.png�hv}�hxj7  suh+hfhj'  hh,hK ubhz)��}�(h��A similar plot to the one above, but now with shaded colors showing
the log10 sensitivity (see :Ref:`sect_sensitivity`) and contours
showing log10 resistivity.�h]�(h�_A similar plot to the one above, but now with shaded colors showing
the log10 sensitivity (see �����}�(h�_A similar plot to the one above, but now with shaded colors showing
the log10 sensitivity (see �hj9  hhhNhNubj�  )��}�(h�:Ref:`sect_sensitivity`�h]�j�  )��}�(hjD  h]�h�sect_sensitivity�����}�(hhhjF  hhhNhNubah}�(h!]�h#]�(j�  �std��std-ref�eh%]�h']�h)]�uh+j�  hjB  ubah}�(h!]�h#]�h%]�h']�h)]��refdoc�j�  �	refdomain�jP  �reftype��ref��refexplicit���refwarn��j�  �sect_sensitivity�uh+j�  hh,hK\hj9  ubh�)) and contours
showing log10 resistivity.�����}�(h�)) and contours
showing log10 resistivity.�hj9  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+hyhh,hK\hj'  ubeh}�(h!]��id7�ah#]�h%]�h']�h)]�uh+hahK\hj   ubhb)��}�(hhh]�(hg)��}�(hX�  .. figure::  _static/images/examples/Serpent1.15.response.png
   :width: 100%

   Example screen shot of CSEM response plot made with the
   interactive MATLAB code `plotMARE2DEM_CSEM.m`. Upper row: symbols
   show the amplitude and phase response at three frequencies for a
   single receiver along with the corresponding model response (black
   lines). Lower row: normalized residuals for the data fit.
�h]�h}�(h!]�h#]�h%]�h']�h)]��width��100%��uri��0_static/images/examples/Serpent1.15.response.png�hv}�hxj�  suh+hfhjt  hh,hK ubhz)��}�(hX7  Example screen shot of CSEM response plot made with the
interactive MATLAB code `plotMARE2DEM_CSEM.m`. Upper row: symbols
show the amplitude and phase response at three frequencies for a
single receiver along with the corresponding model response (black
lines). Lower row: normalized residuals for the data fit.�h]�(h�PExample screen shot of CSEM response plot made with the
interactive MATLAB code �����}�(h�PExample screen shot of CSEM response plot made with the
interactive MATLAB code �hj�  hhhNhNubh	�title_reference���)��}�(h�`plotMARE2DEM_CSEM.m`�h]�h�plotMARE2DEM_CSEM.m�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj�  ubh��. Upper row: symbols
show the amplitude and phase response at three frequencies for a
single receiver along with the corresponding model response (black
lines). Lower row: normalized residuals for the data fit.�����}�(h��. Upper row: symbols
show the amplitude and phase response at three frequencies for a
single receiver along with the corresponding model response (black
lines). Lower row: normalized residuals for the data fit.�hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+hyhh,hKdhjt  ubeh}�(h!]��id8�ah#]�h%]�h']�h)]�uh+hahKdhj   ubhb)��}�(hhh]�(hg)��}�(hXV  .. figure::  _static/images/examples/Serpent1.15.misfitbreakdown.png
   :width: 100%

   Example screen shot of CSEM misfit breakdown plot made with the
   interactive MATLAB code `plotMARE2DEM_CSEM.m`. The misfit breakdown
   plot is useful for showing how well the data is fit as a function of
   receiver position, transmitter position, frequency, data type and
   transmitter-receiver range. Regions with large misfits can indicate
   where they data may be noisy or have problems, or where the model
   mesh needs to be refined to allow for smaller scale features
   required to fit the data.
�h]�h}�(h!]�h#]�h%]�h']�h)]��width��100%��uri��7_static/images/examples/Serpent1.15.misfitbreakdown.png�hv}�hxj�  suh+hfhj�  hh,hK ubhz)��}�(hX�  Example screen shot of CSEM misfit breakdown plot made with the
interactive MATLAB code `plotMARE2DEM_CSEM.m`. The misfit breakdown
plot is useful for showing how well the data is fit as a function of
receiver position, transmitter position, frequency, data type and
transmitter-receiver range. Regions with large misfits can indicate
where they data may be noisy or have problems, or where the model
mesh needs to be refined to allow for smaller scale features
required to fit the data.�h]�(h�XExample screen shot of CSEM misfit breakdown plot made with the
interactive MATLAB code �����}�(h�XExample screen shot of CSEM misfit breakdown plot made with the
interactive MATLAB code �hj�  hhhNhNubj�  )��}�(h�`plotMARE2DEM_CSEM.m`�h]�h�plotMARE2DEM_CSEM.m�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj�  ubhXz  . The misfit breakdown
plot is useful for showing how well the data is fit as a function of
receiver position, transmitter position, frequency, data type and
transmitter-receiver range. Regions with large misfits can indicate
where they data may be noisy or have problems, or where the model
mesh needs to be refined to allow for smaller scale features
required to fit the data.�����}�(hXz  . The misfit breakdown
plot is useful for showing how well the data is fit as a function of
receiver position, transmitter position, frequency, data type and
transmitter-receiver range. Regions with large misfits can indicate
where they data may be noisy or have problems, or where the model
mesh needs to be refined to allow for smaller scale features
required to fit the data.�hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+hyhh,hKmhj�  ubeh}�(h!]��id9�ah#]�h%]�h']�h)]�uh+hahKmhj   ubeh}�(h!]�h#]�h%]�h']�h)]�uh+h\hh�hhhNhNubeh}�(h!]��5middle-american-trench-offshore-nicaragua-mt-and-csem�ah#]�h%]��5middle american trench offshore nicaragua mt and csem�ah']�h)]�uh+h
hhhhhh,hK7ubh)��}�(hhh]�(h)��}�(h� Gemini Salt Body, Gulf of Mexico�h]�h� Gemini Salt Body, Gulf of Mexico�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hKwubh])��}�(hhh]�hb)��}�(hhh]�(hg)��}�(hX�  .. figure::  _static/images/examples/Gemini_LineI_TE.6.resistivity.png
   :width: 100%

   Inversion of marine MT data from Line I at Gemini Prospect, Gulf of
   Mexico. This inversion relaxed the roughness penalty along the
   seismically imaged top of the Gemini salt body (white line),
   allowing for a sharp jump in inverted resistivity. Penalty cuts like
   this are easily created in MARE2DEM's graphical user interface
   Mamba2D.m. Modified from Key, K. W., Constable, S. C., & Weiss, C.
   J. (2006), Mapping 3D salt using the 2D marine magnetotelluric
   method: Case study from Gemini Prospect, Gulf of Mexico. Geophysics,
   71(1), B17–B27. DOI: `10.1190/1.2168007
   <https://doi.org/10.1190/1.2168007>`_.

�h]�h}�(h!]�h#]�h%]�h']�h)]��width��100%��uri��9_static/images/examples/Gemini_LineI_TE.6.resistivity.png�hv}�hxj  suh+hfhj  hh,hK ubhz)��}�(hX[  Inversion of marine MT data from Line I at Gemini Prospect, Gulf of
Mexico. This inversion relaxed the roughness penalty along the
seismically imaged top of the Gemini salt body (white line),
allowing for a sharp jump in inverted resistivity. Penalty cuts like
this are easily created in MARE2DEM's graphical user interface
Mamba2D.m. Modified from Key, K. W., Constable, S. C., & Weiss, C.
J. (2006), Mapping 3D salt using the 2D marine magnetotelluric
method: Case study from Gemini Prospect, Gulf of Mexico. Geophysics,
71(1), B17–B27. DOI: `10.1190/1.2168007
<https://doi.org/10.1190/1.2168007>`_.�h]�(hX$  Inversion of marine MT data from Line I at Gemini Prospect, Gulf of
Mexico. This inversion relaxed the roughness penalty along the
seismically imaged top of the Gemini salt body (white line),
allowing for a sharp jump in inverted resistivity. Penalty cuts like
this are easily created in MARE2DEM’s graphical user interface
Mamba2D.m. Modified from Key, K. W., Constable, S. C., & Weiss, C.
J. (2006), Mapping 3D salt using the 2D marine magnetotelluric
method: Case study from Gemini Prospect, Gulf of Mexico. Geophysics,
71(1), B17–B27. DOI: �����}�(hX"  Inversion of marine MT data from Line I at Gemini Prospect, Gulf of
Mexico. This inversion relaxed the roughness penalty along the
seismically imaged top of the Gemini salt body (white line),
allowing for a sharp jump in inverted resistivity. Penalty cuts like
this are easily created in MARE2DEM's graphical user interface
Mamba2D.m. Modified from Key, K. W., Constable, S. C., & Weiss, C.
J. (2006), Mapping 3D salt using the 2D marine magnetotelluric
method: Case study from Gemini Prospect, Gulf of Mexico. Geophysics,
71(1), B17–B27. DOI: �hj   hhhNhNubh�)��}�(h�8`10.1190/1.2168007
<https://doi.org/10.1190/1.2168007>`_�h]�h�10.1190/1.2168007�����}�(h�10.1190/1.2168007�hj)  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��name�j1  h��!https://doi.org/10.1190/1.2168007�uh+h�hj   ubh�)��}�(h�$
<https://doi.org/10.1190/1.2168007>�h]�h}�(h!]��id2�ah#]�h%]��10.1190/1.2168007�ah']�h)]��refuri�j9  uh+h�h�Khj   ubh�.�����}�(hh�hj   hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+hyhh,hK|hj  ubeh}�(h!]��id10�ah#]�h%]�h']�h)]�uh+hahK|hj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h\hj�  hhhNhNubeh}�(h!]��gemini-salt-body-gulf-of-mexico�ah#]�h%]�� gemini salt body, gulf of mexico�ah']�h)]�uh+h
hhhhhh,hKwubeh}�(h!]��	real-data�ah#]�h%]��	real data�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_images���embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��key16�]�h	�citation_reference���)��}�(h�[Key16]_�h]�h�Key16�����}�(hhhj�  ubah}�(h!]�j�  ah#]�h%]�h']�h)]��refname�j�  uh+j�  hj�  ubas�refids�}��nameids�}�(jk  jh  h�h�h�h�j�  j�  jx  ju  j  j  jc  j`  jC  j@  u�	nametypes�}�(jk  Nh�Nh��j�  Njx  �j  �jc  NjC  �uh!}�(jh  hh�hKh�h�j�  h�ju  jo  j�  j�  j  j	  j`  j�  j@  j:  h�hch�h�j�  j  j"  j�  jo  j'  j�  jt  j�  j�  jS  j  u�footnote_refs�}��citation_refs�}�j�  ]�j�  as�autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}�j�  K
s��R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.