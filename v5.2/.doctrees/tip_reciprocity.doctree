���2      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�,Reciprocity for Faster Modeling of CSEM Data�h]�h	�Text����,Reciprocity for Faster Modeling of CSEM Data�����}�(hh�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh��/var/folders/dj/ywf9rt4x4g90mjt155y6r7vh0000gn/T/tmpfki2_mdt/edff908df8016d7ccbbfd68b0adb77562f4299c2/source/tip_reciprocity.rst�hKubh	�	paragraph���)��}�(h��Here we just state some quick tips about when to use reciprocity to
speed up CSEM computations; see the :ref:`sect_reciprocity` section for
the theoretical background and more details.�h]�(h�hHere we just state some quick tips about when to use reciprocity to
speed up CSEM computations; see the �����}�(h�hHere we just state some quick tips about when to use reciprocity to
speed up CSEM computations; see the �hh/hhhNhNubh �pending_xref���)��}�(h�:ref:`sect_reciprocity`�h]�h	�inline���)��}�(hh<h]�h�sect_reciprocity�����}�(hhhh@hhhNhNubah}�(h!]�h#]�(�xref��std��std-ref�eh%]�h']�h)]�uh+h>hh:ubah}�(h!]�h#]�h%]�h']�h)]��refdoc��tip_reciprocity��	refdomain�hK�reftype��ref��refexplicit���refwarn���	reftarget��sect_reciprocity�uh+h8hh,hKhh/ubh�9 section for
the theoretical background and more details.�����}�(h�9 section for
the theoretical background and more details.�hh/hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh.)��}�(hX�  The run time for MARE2DEM depends strongly on the number of CSEM
transmitters being modeled and less so on the number of receivers. Thus,
when the number of transmitters greatly exceeds the number of receivers
(more specifically, the number of receivers times the numbers of
receiver components being modeled) then electromagnetic reciprocity
should be applied to the input data file so that MARE2DEM can solve the
reciprocal problem more quickly.�h]�hX�  The run time for MARE2DEM depends strongly on the number of CSEM
transmitters being modeled and less so on the number of receivers. Thus,
when the number of transmitters greatly exceeds the number of receivers
(more specifically, the number of receivers times the numbers of
receiver components being modeled) then electromagnetic reciprocity
should be applied to the input data file so that MARE2DEM can solve the
reciprocal problem more quickly.�����}�(hhlhhjhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh)��}�(hhh]�(h)��}�(h� When should I apply reciprocity?�h]�h� When should I apply reciprocity?�����}�(hh}hh{hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhhxhhhh,hKubh)��}�(hhh]�(h)��}�(h�Yes�h]�h�Yes�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh�hhhh,hKubh	�bullet_list���)��}�(hhh]�h	�	list_item���)��}�(hX  When the number of transmitters greatly exceeds the number of receivers times the number
of components (per receiver). Reciprocity is almost always recommended for nodal seafloor
marine CSEM data since there are usually far fewer receivers than there are transmitter
*shot* points.
�h]�h.)��}�(hX  When the number of transmitters greatly exceeds the number of receivers times the number
of components (per receiver). Reciprocity is almost always recommended for nodal seafloor
marine CSEM data since there are usually far fewer receivers than there are transmitter
*shot* points.�h]�(hX  When the number of transmitters greatly exceeds the number of receivers times the number
of components (per receiver). Reciprocity is almost always recommended for nodal seafloor
marine CSEM data since there are usually far fewer receivers than there are transmitter
�����}�(hX  When the number of transmitters greatly exceeds the number of receivers times the number
of components (per receiver). Reciprocity is almost always recommended for nodal seafloor
marine CSEM data since there are usually far fewer receivers than there are transmitter
�hh�hhhNhNubh	�emphasis���)��}�(h�*shot*�h]�h�shot�����}�(hhhh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�ubh� points.�����}�(h� points.�hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�hhhh,hNubah}�(h!]�h#]�h%]�h']�h)]��bullet��-�uh+h�hh,hKhh�hhubeh}�(h!]��yes�ah#]�h%]��yes�ah']�h)]�uh+h
hhxhhhh,hKubh)��}�(hhh]�(h)��}�(h�No�h]�h�No�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh�hhhh,hKubh�)��}�(hhh]�(h�)��}�(h�vFor land CSEM data, usually there are far fewer transmitters
than receivers and so reciprocity should not be applied.
�h]�h.)��}�(h�uFor land CSEM data, usually there are far fewer transmitters
than receivers and so reciprocity should not be applied.�h]�h�uFor land CSEM data, usually there are far fewer transmitters
than receivers and so reciprocity should not be applied.�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�hhhh,hNubh�)��}�(h��For towed streamer CSEM data, the source moves with the attached towed receivers and so
reciprocity should not be applied since it offers no speed up advantage.

�h]�h.)��}�(h��For towed streamer CSEM data, the source moves with the attached towed receivers and so
reciprocity should not be applied since it offers no speed up advantage.�h]�h��For towed streamer CSEM data, the source moves with the attached towed receivers and so
reciprocity should not be applied since it offers no speed up advantage.�����}�(hj  hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK!hj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�hhhh,hNubeh}�(h!]�h#]�h%]�h']�h)]�h�h�uh+h�hh,hKhh�hhubeh}�(h!]��no�ah#]�h%]��no�ah']�h)]�uh+h
hhxhhhh,hKubeh}�(h!]��when-should-i-apply-reciprocity�ah#]�h%]�� when should i apply reciprocity?�ah']�h)]�uh+h
hhhhhh,hKubh)��}�(hhh]�(h)��}�(h�How do I apply reciprocity?�h]�h�How do I apply reciprocity?�����}�(hj>  hj<  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj9  hhhh,hK&ubh.)��}�(hX3  The geometry (location and angles) of the source and receiver wires
must be maintained for EM reciprocity to hold; thus a dipping transmitter source
becomes an identically dipping receiver at the same location, and likewise a real
tilted receiver will become an identically tilted (and located) transmitter.�h]�hX3  The geometry (location and angles) of the source and receiver wires
must be maintained for EM reciprocity to hold; thus a dipping transmitter source
becomes an identically dipping receiver at the same location, and likewise a real
tilted receiver will become an identically tilted (and located) transmitter.�����}�(hjL  hjJ  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK(hj9  hhubh.)��}�(hX=  An easy way to think about this is to consider the actual transmitter wire and the
actual receiver wire. Leave them as is (same location, same rotation and tilt angles),
and just reverse energize them, so that the source wire becomes  a receiver wire and
the actual receiver wire becomes energized as the source wire.�h]�hX=  An easy way to think about this is to consider the actual transmitter wire and the
actual receiver wire. Leave them as is (same location, same rotation and tilt angles),
and just reverse energize them, so that the source wire becomes  a receiver wire and
the actual receiver wire becomes energized as the source wire.�����}�(hjZ  hjX  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK-hj9  hhubh.)��}�(hXf  The easiest case is for electric sources and receivers as shown in the
image below. If the receiver was instead a magnetic receiver component
(say the crossline magnetic field component), then the actual receiver
becomes a magnetic source (in the same direction and location as the actual receiver component)
and the actual sources become electric receivers.�h]�hXf  The easiest case is for electric sources and receivers as shown in the
image below. If the receiver was instead a magnetic receiver component
(say the crossline magnetic field component), then the actual receiver
becomes a magnetic source (in the same direction and location as the actual receiver component)
and the actual sources become electric receivers.�����}�(hjh  hjf  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK2hj9  hhubh	�block_quote���)��}�(hhh]�h	�figure���)��}�(hhh]�(h	�image���)��}�(hX�  .. figure::  _static/images/reciprocity.png
   :width: 100%

   Applying reciprocity to reduce the number of numerical transmitters
   and thus speed up MARE2DEM. The top image shows the actual survey
   geometry, where a single seafloor receiver recorded  data points for
   each of five different transmitter locations. This requires solving
   five linear systems in MARE2DEM. The bottom image shows the
   equivalent responses that can be generated through EM reciprocity,
   where the five sources are turned into five receivers and the
   original receiver is turned into a single transmitter, thus
   requiring only a single linear system to be solved.
�h]�h}�(h!]�h#]�h%]�h']�h)]��width��100%��uri��_static/images/reciprocity.png��
candidates�}��*�j�  suh+j~  hj{  hh,hK ubh	�caption���)��}�(hX;  Applying reciprocity to reduce the number of numerical transmitters
and thus speed up MARE2DEM. The top image shows the actual survey
geometry, where a single seafloor receiver recorded  data points for
each of five different transmitter locations. This requires solving
five linear systems in MARE2DEM. The bottom image shows the
equivalent responses that can be generated through EM reciprocity,
where the five sources are turned into five receivers and the
original receiver is turned into a single transmitter, thus
requiring only a single linear system to be solved.�h]�hX;  Applying reciprocity to reduce the number of numerical transmitters
and thus speed up MARE2DEM. The top image shows the actual survey
geometry, where a single seafloor receiver recorded  data points for
each of five different transmitter locations. This requires solving
five linear systems in MARE2DEM. The bottom image shows the
equivalent responses that can be generated through EM reciprocity,
where the five sources are turned into five receivers and the
original receiver is turned into a single transmitter, thus
requiring only a single linear system to be solved.�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hh,hK;hj{  ubeh}�(h!]��id1�ah#]�h%]�h']�h)]�uh+jy  hK;hjv  ubah}�(h!]�h#]�h%]�h']�h)]�uh+jt  hj9  hhhNhNubeh}�(h!]��how-do-i-apply-reciprocity�ah#]�h%]��how do i apply reciprocity?�ah']�h)]�uh+h
hhhhhh,hK&ubeh}�(h!]��,reciprocity-for-faster-modeling-of-csem-data�ah#]�h%]��,reciprocity for faster modeling of csem data�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_images���embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j�  j�  j6  j3  h�h�j.  j+  j�  j�  u�	nametypes�}�(j�  Nj6  Nh�Nj.  Nj�  Nuh!}�(j�  hj3  hxh�h�j+  h�j�  j9  j�  j{  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}�j�  Ks��R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.